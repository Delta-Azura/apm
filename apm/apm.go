package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"

	//"path/filepath"
	"strings"
)

func build() {
	args := os.Args[2:]
	arg := strings.Join(args, " ")
	to_build := strings.Split(arg, " ")
	number := len(args)
	fmt.Println(number)
	for i := 1; i < number+1; i++ {
		list := to_build[len(to_build)-i]
		fmt.Println("Building", list)
		_, err := os.Stat("/etc/apm/apm.conf")
		if err == nil {
			fmt.Println("Configuration settings are on !")
		} else {
			fmt.Println("Need")
			os.Mkdir("/etc/apm/", 0755)
			os.Mkdir("/usr/share/apm/", 0755)
			os.Mkdir("/usr/local/apm", 0755)
			os.Create("/etc/apm/vpm.conf")
			fmt.Println("All configuration settings are on")
		}
		path := fmt.Sprintf("/usr/share/apm/%s/", list)
		os.Chdir(path)
		os.Chdir("info")
		release, _ := ioutil.ReadFile("release")
		depends, _ := ioutil.ReadFile("depends")
		makedepends, _ := ioutil.ReadFile("makedepends")
		packager, _ := ioutil.ReadFile("packager")
		sources, err := ioutil.ReadFile("sources")
		if err != nil {
			fmt.Println("Sources doesn't exist")
			os.Exit(2)
		}
		os.Chdir(path)
		_, err = os.Stat("build")
		build, _ := ioutil.ReadFile("build")
		if err != nil {
			fmt.Println("Build script is not present : Aborting")
			os.Exit(2)
		}
		url := string(sources)
		source := strings.Split(url, "\n")
		justString := strings.Join(source, "")
		file := strings.Split(justString, "/")
		filename := file[len(file)-1]
		fmt.Println(filename)
		fmt.Println(justString)
		splitting := strings.Split(justString, ".")
		ext := splitting[len(splitting)-1]
		cache := fmt.Sprintf("/var/cache/apm/%s", list)
		os.MkdirAll(cache, 0755)
		os.Chdir(cache)
		_, url_error := exec.Command("wget", justString).Output()
		if url_error != nil {
			fmt.Println("The source link is not correct")
			os.Exit(2)
		}
		fmt.Println(ext)
		if ext == "tar" {
			tar := "tar"
			perm := "–xvf"
			exec.Command(tar, perm, filename).Output()
		}
		if ext == "zip" {
			//unzip := "unzip"
			_, err := exec.Command("unzip", filename).Output()
			if err != nil {
				fmt.Println(err)
			}
		}
		if ext == "gz" {
			tar := "tar"
			perm := "-xf"
			exec.Command(tar, perm, filename).Output()
		}
		director := fmt.Sprintf("%s-%s", list, release)
		dir_name := strings.ReplaceAll(director, "\n", "")
		err_dir := os.Chdir(dir_name)
		if err_dir != nil {
			fmt.Println(err_dir)
		}
		os.Create("build")
		ioutil.WriteFile("build", build, 0755)
		exec.Command("chmod", "u+x", "build").Output()
		cmd, err := exec.Command("/bin/bash", "build").Output()
		out := string(cmd)
		fmt.Println(out)
		if err != nil {
			fmt.Printf("error %s", err)
		}
		_, err = os.Stat("postinstall")
		if err != nil {
			fmt.Println("Postinstall not needed")
		} else {
			exec.Command("chmod", "u+x", "postinstall")
			postinstall, err := exec.Command("/bin/bash", "/usr/share/vpm/postinstall").Output()
			if err != nil {
				fmt.Println("error")
			} else {
				fmt.Println(postinstall)
			}
		}
		destination := fmt.Sprintf("/usr/local/apm/%s/", list)
		os.MkdirAll(destination, 0755)
		os.Chdir(destination)
		err_dest := os.MkdirAll("info", 0755)
		if err_dest != nil {
			fmt.Println(err_dest)
		}
		os.Chdir("info")
		os.Create("release")
		os.Create("packager")
		os.Create("sources")
		os.Create("depends")
		os.Create("makedepends")
		ioutil.WriteFile("release", release, 0755)
		ioutil.WriteFile("packager", packager, 0755)
		ioutil.WriteFile("sources", sources, 0755)
		ioutil.WriteFile("depends", depends, 0755)
		ioutil.WriteFile("makedepends", makedepends, 0755)
		fmt.Println("Done")
	}
}

func info() {
	name := os.Args[2]
	location := fmt.Sprintf("/usr/local/apm/%s/info/", name)
	os.Chdir(location)
	release, _ := ioutil.ReadFile("release")
	packager, _ := ioutil.ReadFile("packager")
	sources, _ := ioutil.ReadFile("sources")
	fmt.Println("Informations about :", name, "\n")
	fmt.Println("Packager : ", string(packager))
	fmt.Println("Release : ", string(release))
	fmt.Println("Sources : ", string(sources))

}

func remove() {
	args := os.Args[2:]
	number := len(args)
	if number < 2 {
		arg := strings.Join(args, " ")
		vpm := fmt.Sprintf("/usr/local/apm/%s", arg)
		bin := fmt.Sprintf("/usr/bin/%s", arg)
		share := fmt.Sprintf("/usr/share/apm/%s", arg)
		lib := fmt.Sprintf("/usr/lib/%s", arg)
		lib64 := fmt.Sprintf("/usr/lib64/%s", arg)
		os.RemoveAll(vpm)
		os.RemoveAll(bin)
		os.RemoveAll(share)
		os.RemoveAll(lib)
		os.RemoveAll(lib64)
	} else {
		for i := 1; i < number+1; i++ {
			os.Chdir("/usr/local/apm/")
			arg := strings.Join(args, " ")
			to_remove := strings.Split(arg, " ")
			os.Mkdir("cache", 0755)
			list := to_remove[len(to_remove)-i]
			vpm := fmt.Sprintf("/usr/local/apm/%s", list)
			bin := fmt.Sprintf("/usr/bin/%s", list)
			share := fmt.Sprintf("/usr/share/apm/%s", list)
			lib := fmt.Sprintf("/usr/lib/%s", list)
			lib64 := fmt.Sprintf("/usr/lib64/%s", list)
			os.RemoveAll(vpm)
			os.RemoveAll(bin)
			os.RemoveAll(share)
			os.RemoveAll(lib)
			os.RemoveAll(lib64)
			removed := fmt.Sprintf("The package %s was removed !", list)
			fmt.Println(removed)
		}
	}

}
func update() {
	os.Chdir("/usr/local/apm")
	_, err := os.Stat("/var/cache/vpmbuild")
	if err != nil {
		os.Chdir("/var/cache/")
		exec.Command("git", "clone", "https://gitlab.com/Delta-Azura/vpmbuild.git").Output()
	} else {
		os.Chdir("vpmbuild/")
		exec.Command("git", "pull").Output()
	}
	os.Chdir("/usr/local/apm")
	out, _ := exec.Command("ls").Output()
	output := string(out)
	split := strings.Split(output, "\n")
	number := len(split)
	for i := 2; i < number+1; i++ {
		list := split[len(split)-i]
		apm := fmt.Sprintf("/usr/local/apm/%s/info", list)
		os.Chdir(apm)
		local, _ := ioutil.ReadFile("release")
		git := fmt.Sprintf("/var/cache/vpmbuild/src/%s/info", list)
		apmbuild := fmt.Sprintf("/var/cache/vpmbuild/src/%s/", list)
		os.Chdir(git)
		fmt.Println(list)
		distant, _ := ioutil.ReadFile("release")
		local_string := string(local)
		distant_string := string(distant)
		fmt.Println(local_string)
		fmt.Println(distant_string)
		if local_string != distant_string {
			exec.Command("cp", "-rf", apmbuild, "/usr/share/apm/").Output()
		}
	}
	arg := strings.Join(split, " ")
	fmt.Printf("To complete update please run sudo apm build %s", arg)
}

func main() {
	arg := os.Args[1]
	if arg == "build" {
		build()
	}
	if arg == "info" {
		info()
	}
	if arg == "remove" {
		remove()
	}
	if arg == "update" {
		update()
	}
}
